#!/usr/bin/make -f

include /usr/share/javahelper/java-vars.mk
include /usr/share/dpkg/pkg-info.mk

export LEIN_HOME=$(CURDIR)/.lein
export LEIN_OFFLINE=true
NAME=trapperkeeper-metrics

%:
	dh $@ --with javahelper --with maven_repo_helper

override_dh_auto_configure:
	cd debian && ln -sf /usr/share/maven-repo .

override_dh_auto_build:
	# "with-profile -dev" below is a workaround for
	# https://codeberg.org/leiningen/leiningen/issues/14
	# upstream needs to fix their project.clj
	lein with-profile -dev pom debian/pom.xml
	lein jar
	# symlink so we don't need a version in debian/*.poms
	cd target && ln -sf $(NAME)-$(DEB_VERSION_UPSTREAM).jar $(NAME).jar
	cd target/test && ln -sf $(NAME)-$(DEB_VERSION_UPSTREAM)-test.jar $(NAME)-test.jar

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	cp debian/gen-pki.sh dev-resources
	cp debian/gen-pki.exts.cnf dev-resources/exts.cnf
	./dev-resources/gen-pki.sh
	lein test
endif

override_dh_clean:
	rm -f debian/maven-repo
	rm -Rf target
	rm -f debian/pom.xml
	dh_clean
